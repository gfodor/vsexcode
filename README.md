To generate the documentation:

* Set up docco: http://jashkenas.github.io/docco/
* In the `SingleRoomGL` directory, run:

  docco src/*.c src/constants.h
