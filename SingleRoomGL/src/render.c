// The `render.c` file defines functions used to render the scene. 
// It is left largely undocumented, since implementing these functions is largely irrelevant to using the 3D audio engine. 

#include "render.h"
#include "texture-data.h"

// Global state
// ------------

// Material colors.
GLfloat alt_material_color[4] = { 0.932f, 0.588f, 0.478f, 0.0f };
GLfloat white_material_color[4] = { 1.000f, 1.000f, 1.000f, 0.0f };

// Light colors.
GLfloat light_diffuse[4] = {0.3f, 0.7f, 0.3f, 1.0f};
GLfloat light_ambient[4] = {0.5f, 0.5f, 0.5f, 1.0f};
GLfloat light_specular[4] = {0.1f, 0.2f, 0.1f, 1.0f};
GLfloat light_position[4] = {1.0f, 1.0f, 1.0f, 0.0f};

// OpenGL texture IDs.
GLuint gl_textures[6];

// **init_gl** sets up the initial GL scene and viewport.
void init_gl(void)
{
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize(640, 480);

    glutCreateWindow("Visisonics Audio Engine Demo");

    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    glewInit();
    glGenTextures(6, gl_textures);

    // These textures are made available via statically allocated arrays defined in `texture-data.h`.
    unsigned char *texture_datas[6] = { _pztrain, _porange, _pbarbie, _chinese, _parquet, _popcorn };

    for (int i = 0; i < 6; i++)
    {
        glBindTexture(GL_TEXTURE_2D, gl_textures[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _TEXTURE_SIZE, _TEXTURE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_datas[i]);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, 4.0 / 3.0, _MIN_DRAW_RANGE, _MAX_DRAW_RANGE);
    glMatrixMode(GL_MODELVIEW);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

// **draw_room** performs the OpenGL draw commands to draw the walls of the room.
void draw_room(void)
{
    glMaterialfv(GL_FRONT, GL_SPECULAR, white_material_color);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, white_material_color);
    glMaterialfv(GL_FRONT, GL_AMBIENT, white_material_color);
    glEnable(GL_TEXTURE_2D);

    // Draw left wall.
    glBindTexture(GL_TEXTURE_2D, gl_textures[0]);
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glTexCoord2f( ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glEnd();

    // Draw right wall.
    glBindTexture(GL_TEXTURE_2D, gl_textures[1]);
    glBegin(GL_QUADS);
    glNormal3f( 1.0f, 0.0f, 0.0f);
    glTexCoord2f( ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glEnd();

    // Draw back wall.
    glBindTexture(GL_TEXTURE_2D, gl_textures[2]);
    glBegin(GL_QUADS);
    glNormal3f( 0.0f, 0.0f, 1.0f);
    glTexCoord2f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glEnd();

    // Draw front wall.
    glBindTexture(GL_TEXTURE_2D, gl_textures[3]);
    glBegin(GL_QUADS);
    glNormal3f( 0.0f, 0.0f, -1.0f);
    glTexCoord2f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glEnd();

    // Draw floor.
    glBindTexture(GL_TEXTURE_2D, gl_textures[4]);
    glBegin(GL_QUADS);
    glNormal3f( 0.0f, 1.0f, 0.0f);
    glTexCoord2f(- ROOM_WIDTH / 2, - ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, - ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_WIDTH / 2, ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, - ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glEnd();

    // Draw ceiling.
    glBindTexture(GL_TEXTURE_2D, gl_textures[5]);
    glBegin(GL_QUADS);
    glNormal3f( 0.0f, -1.0f, 0.0f);
    glTexCoord2f(- ROOM_WIDTH / 2, - ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, - ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, - ROOM_LENGTH / 2);
    glTexCoord2f( ROOM_WIDTH / 2, ROOM_LENGTH / 2);
    glVertex3f( ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glTexCoord2f(- ROOM_WIDTH / 2, ROOM_LENGTH / 2);
    glVertex3f(- ROOM_WIDTH / 2, ROOM_HEIGHT / 2, ROOM_LENGTH / 2);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    return;
}

// **draw_room** performs the OpenGL draw commands to draw the sphere representing the sound source.
void draw_source(float source_x, float source_y, float source_z)
{
    GLUquadric *_quad = gluNewQuadric();
    glTranslatef(source_x, source_y, source_z);

    glMaterialfv(GL_FRONT, GL_AMBIENT, alt_material_color);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, alt_material_color);
    glMaterialfv(GL_FRONT, GL_SPECULAR, alt_material_color);

    gluQuadricDrawStyle(_quad, GLU_FILL);
    gluQuadricNormals(_quad, GLU_SMOOTH);

    gluSphere(_quad, 0.1, 32, 32);
    gluDeleteQuadric(_quad);
}
