// The `main.c` file defines the global state containing the camera position/orientation and the sound source position, 
// and defines the keyboard event handlers for the user to move the camera and source.

// It also of course provides the `main()` function, which initializes the 
// audio engine, starts the audio processing thread, and starts the OpenGL rendering loop.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	PTW32_STATIC_LIB

#include "pthread.h"

#include "sound_exec.h"
#include "render.h"
#include "constants.h"

// Global state
// ------------

// Thread handle for the background audio processing thread.
// The thread handler `audio_thread_exec` can be found in [sound_exec.c](sound_exec.html).
pthread_t audio_thread_handle;

// Current camera X, Y, Z in world space in meters. The camera is initialized to be centered
// horizontally within the room with a Y and Z offset to simulate a player standing in 
// the front of the room looking in.
float camera_x = 0.0f;
float camera_y = -ROOM_HEIGHT / 2.0f + PLAYER_HEIGHT;
float camera_z = -2.5f;

// Current camera orientation psi (azimuth) and theta (altitude) angles in degrees.
float camera_psi = 0.0f, camera_theta = 0.0f;

// Current sound source coordinates in world space in meters.
float source_x = 0.3f, source_y = -0.4f, source_z = 0.0f;

// Function definitions
// --------------------

// **bound_camera_and_source_to_room** repositions the camera and source to lie inside the room if 
// they have been set out of bounds.
void bound_camera_and_source_to_room(void)
{
    // For each of the x, y, z coordinates of the camera and source, we clip them to be 
    // inside of the max, min coordinate of the appropriate room plane.
    camera_x = fminf(fmaxf(camera_x, -ROOM_WIDTH / 2 + MINIMUM_WALL_DISTANCE), ROOM_WIDTH / 2 - MINIMUM_WALL_DISTANCE);
    camera_y = fminf(fmaxf(camera_y, -ROOM_HEIGHT / 2 + MINIMUM_WALL_DISTANCE), ROOM_HEIGHT / 2 - MINIMUM_WALL_DISTANCE);
    camera_z = fminf(fmaxf(camera_z, -ROOM_LENGTH / 2 + MINIMUM_WALL_DISTANCE), ROOM_LENGTH / 2 - MINIMUM_WALL_DISTANCE);
    source_x = fminf(fmaxf(source_x, -ROOM_WIDTH / 2 + MINIMUM_WALL_DISTANCE), ROOM_WIDTH / 2 - MINIMUM_WALL_DISTANCE);
    source_y = fminf(fmaxf(source_y, -ROOM_HEIGHT / 2 + MINIMUM_WALL_DISTANCE), ROOM_HEIGHT / 2 - MINIMUM_WALL_DISTANCE);
    source_z = fminf(fmaxf(source_z, -ROOM_LENGTH / 2 + MINIMUM_WALL_DISTANCE), ROOM_LENGTH / 2 - MINIMUM_WALL_DISTANCE);
}

// **main_keyboard_handler** is the keyboard event handler which handles all keys except the arrow 
// keys, which requires a separate handler (see below.) In this event handler, we update the camera
// position along two axes in response to WASD keyboard commands, and update the source 
// along three axes via UHJKYI keyboard commands.
void main_keyboard_handler(unsigned char key, int x, int y)
{
    // Compute change of basis vectors in order to transform the camera in world space
    // based upon motion along its local Z and X axes given its orientation.
    float world_x_to_camera_z = (float)cos(M_PI * camera_psi / 180.0);
    float world_z_to_camera_z = (float)-sin(M_PI * camera_psi / 180.0);
    float world_x_to_camera_x = (float)sin(M_PI * camera_psi / 180.0);
    float world_z_to_camera_x = (float)cos(M_PI * camera_psi / 180.0);

    // In response to keyboard commands from the user, transform the camera along its local 
    // axis (to simulate 'moving around') by using the transformation vectors above, and transform
    // the source in world space directly.
    switch(key)
    {
    case 'a': case 'A': 
        camera_x += 0.1f * world_x_to_camera_z;
        camera_z += 0.1f * world_z_to_camera_z;
        break;
    case 'd': case 'D': 
        camera_x -= 0.1f * world_x_to_camera_z;
        camera_z -= 0.1f * world_z_to_camera_z;
        break;
    case 'w': case 'W': 
        camera_x += 0.1f * world_x_to_camera_x;
        camera_z += 0.1f * world_z_to_camera_x;
        break;
    case 's': case 'S': 
        camera_x -= 0.1f * world_x_to_camera_x;
        camera_z -= 0.1f * world_z_to_camera_x;
        break;
    case 'h': case 'H': 
        source_x += 0.1f;
        break;
    case 'k': case 'K': 
        source_x -= 0.1f;
        break;
    case 'u': case 'U': 
        source_y += 0.1f;
        break;
    case 'j': case 'J': 
        source_y -= 0.1f;
        break;
    case 'y': case 'Y': 
        source_z += 0.1f;
        break;
    case 'i': case 'I': 
        source_z -= 0.1f;
        break;
    case 27: glutLeaveMainLoop();
    }

    // Once the camera or source positions have changed, bound them to be within the room if the user
    // tried to move outside the room, and then sync the new positions with the 3d audio engine.
    // Once the new positions are synced, the next block processed by the audio engine will reflect
    // the updated positions.
    bound_camera_and_source_to_room();

    update_vsengine_source_position(source_x, source_y, source_z);
    update_vsengine_listener_position(camera_x, camera_y, camera_z);

    return;
}

// **special_keyboard_handler** is the keyboard event handler which handles the arrow keys.
// In this event handler, we update the camera orientation in response to up/down/left/right keyboard
// commands.
void special_keyboard_handler(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_RIGHT:
        camera_psi -= 6.25;
        break;
    case GLUT_KEY_LEFT:
        camera_psi += 6.25;
        break;
    case GLUT_KEY_DOWN:
        camera_theta -= 6.25;
        break;
    case GLUT_KEY_UP:
        camera_theta += 6.25;
        break;
    }

    // Once the camera angles are changed, bound the altitude angle such that we can maximally
    // 'look' nearly straight up or straight down (+/- 87.5 degrees.) Then, sync the new orientation
    // with the 3d audio engine. Once the new orientation is synced, the next block processed by the 
    // audio engine will reflect the updated orientation of the listener.
    camera_theta = fmaxf(fminf(camera_theta, 87.5), -87.5);

    update_vsengine_listener_orientation(camera_psi, camera_theta);

    return;
}

// **render** is callback for the render loop. It orients the camera, and then draws the room and 
// sphere representing the source position using OpenGL immediate mode commands.
// The detailed GL drawing calls can be found in `draw_room()` and `draw_source()` in [render.c](render.html).
void render(void)
{
    double target_x = camera_x + sin(M_PI * camera_psi / 180.0) * cos(M_PI * camera_theta / 180.0);
    double target_y = camera_y + sin(M_PI * camera_theta / 180.0);
    double target_z = camera_z + cos(M_PI * camera_psi / 180.0) * cos(M_PI * camera_theta / 180.0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();

    gluLookAt(camera_x, camera_y, camera_z, target_x, target_y, target_z, 0.0, 1.0, 0.0);

    draw_room();
    draw_source(source_x, source_y, source_z);
    glPopMatrix();

    glutSwapBuffers();
    return;
}

// **main** entry point. Initializes audio engine and graphics, and kicks off rendering and audio loops.
int main(int _argc, char **_argv)
{
    // Attach statically linked pthreads to process.
    pthread_win32_process_attach_np();

    // Initialize OpenGL API.
    glutInit(&_argc, _argv);

    // Set up the configuration data structures for the RealSpace3D audio engine. 
    // Defined in [sound_exec.c](sound_exec.html).
    setup_vsengine_config();

    // Ensure the initial camera and source lie within the room.
    bound_camera_and_source_to_room();

    // Initialize the RealSpace3D audio engine's HRTF, sound source, etc. Defined in [sound_exec.c](sound_exec.html).
    init_vsengine();

    // Sync the camera and source position and orientation with the 3D audio engine.
    // This ensures the first audio block processed by the RealSpace3D engine reflects the geometry of the scene.
    update_vsengine_listener_position(camera_x, camera_y, camera_z);
    update_vsengine_listener_orientation(camera_psi, camera_theta);
    update_vsengine_source_position(source_x, source_y, source_z);

    // Initialize the OpenGL scene and viewport. Defined in [render.c](render.html).
    init_gl();

    // Bind the render, idle, and keyboard handlers for OpenGL. The render handler is defined above.
    // The idle handler is just the OpenGL function to redisplay the window. The keyboard handlers, 
    // defined above, allow the user to control the camera and source position via WASD and UHJKYI respectively.
    glutDisplayFunc(render);
    glutIdleFunc(glutPostRedisplay);
    glutKeyboardFunc(main_keyboard_handler);
    glutSpecialFunc(special_keyboard_handler);

    // Spawn the background thread for audio processing by RealSpace3D. `audio_thread_exec` is the thread handler,
    // defined in [sound_exec.c](sound_exec.html).
    pthread_create(&audio_thread_handle, NULL, audio_thread_exec, NULL);

    // Begin the main loop and block until the user exits.
    glutMainLoop();

    // After exiting, signal the audio processing thread to exit and then join on the thread.
    shutdown_audio_thread();
    void *join_ptr;
    pthread_join(audio_thread_handle, &join_ptr);

    // Finally, clean up any auxiliary data structures used by the running audio processing thread.
    cleanup_audio_thread();

    // Detach pthreads.
    pthread_win32_process_detach_np();

    return(0);
}
