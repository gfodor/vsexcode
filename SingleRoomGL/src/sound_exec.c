// The `sound_exec.c` file defines the global state needed for the RealSpace3D sound engine
// and defines functions to initialize the engine, process sound with the engine, and shut down the engine.

// The RealSpace3D SDK is very low level. It is more of a convolution engine rather than a sound library.
// It does not have the ability to actually output sound; the host program is responsible for that, as is done here.
// The SDK supports head tracking but does not interact with head tracker per se; the host code is supposed to 
// provide current head tracking data with each data block submitted for processing.
//
// Generally, the workflow is as follows :
//  * Set configuration data structures for HRTF and source(s) (see `setup_vsengine_config()`.)
//  * Load HRTF (see `init_vsengine()`.)
//  * Allocate scratch area for each source you intend to play (see `init_vsengine()`.)
//  * Populate scratch area for each source you intend to play (see `init_vsengine()`.)
//  * In a loop, process input buffers, obtain output buffers, and play them (see `audio_thread_exec()` and `perform_spatial_audio_transform()`.)
//  * De-allocate scratch area for each source. (see `cleanup_audio_thread()`.)
//  * Unload HRTF. (see `cleanup_audio_thread()`.)

#include "sound_exec.h"

// In this example, the raw HRTF data is provided via a statically allocated array defined in `hrtf-cp048-v003.h`.
#include "hrtf-cp048-v003.h"

// Global state
// ------------

// Flag which is flipped to zero to shut down the audio thread.
int audio_thread_enabled = 1;

// Current "sample index" being processed by the audio engine. Each iteration of the audio
// processing loop processes a single chunk, made up of `SAMPLES_PER_CHUNK` samples, and this index 
// is incremented by that amount at the end of the loop.
int current_source_sample_index;

// Loaded raw source audio data.
float *source_data;

// Number of samples in `source_data`.
int source_length;

// Define the necessary RealSpace3D engine data structures (defined in `vsengine.h`) used in this example:

// * Holds the HRTF.
struct _hrtf_data hrtf;

// * Optional auxiliary data structure used to speed up certain HRTF computations.
struct _hrtf_hash hrtf_hash;

// * Configuration of the single sound source in the scene. `source_info_previous` is an additional 
// `_source_information` structure storing the previous source state, used to reduce artifacts that can result from 
// a moving source or listener. (see `perform_spatial_audio_transform()` for more info.)
struct _source_information source_info, source_info_previous;

// * "Scratch area" used by the engine for the source.
struct _source_work_area source_work_area;

// Mutex used to ensure the engine has a consistent view of the listener and source when processing audio blocks.
pthread_mutex_t position_mutex;

// Function definitions
// --------------------

// **load_source_audio_data** loads the file named in `SOURCE_FILENAME` and stores the number of samples
// and the samples themselves into `source_length` and `source_data`, respectively. The loaded file is resampled to
// `CD_SAMPLES_PER_SECOND` (44khz), if necessary, in order to normalize the sample rate throughout processing.

// The majority of the work to load and resample audio files is dispatched to functions defined in [sound_load.c](sound_load.html).
int load_source_audio_data(void)
{
    printf("preparing source 0 audio data...\n", SOURCE_FILENAME);

    // Determine the number of samples in the file if resampled to 44khz.
    source_length = read_sound_file_length(SOURCE_FILENAME, 0, CD_SAMPLES_PER_SECOND);

    if (source_length == -1) return(-1);

    // Allocate memory and load the file, resampling to 44khz if necessary.
    source_data = (float *)malloc(source_length * sizeof(float));
    if (source_data); else
    {
        puts("malloc failed");
        return(-1);
    }

    if (read_sound_file_data(SOURCE_FILENAME, 0, source_data, CD_SAMPLES_PER_SECOND)) return(-1);

    return(0);
}

// **setup_vsengine_config** sets up the RealSpace3D data structures which specify the HRTF and source configuration.
int setup_vsengine_config()
{
    // The `hrtf` structure (of type `_hrtf_data`, defined in `vsengine.h`) holds the HRTF configuration and data.

    // These next six parameters are used by the HAT model when computing the time delays that are to be applied to 
    // left / right ears. Of the six, the most important is the head radius; the rest have marginal influence.

    // When HRTF interpolation is not used, these parameters are ignored. (The remaining members of `_hrtf_data` are set automatically by `_vsengine_prepare_hrtf()` call.)

    // Radius of the head, in meters.
    hrtf._head_radius = 0.093;
    // Radius of the torso, in meters.
    hrtf._trso_radius = 0.190;
    // Neck height, in meters.
    hrtf._neck_height = 0.053;
    // Undocumented for now, set to value shown.
    hrtf._alpha_min = 0.1;
    // Undocumented for now, set to value shown.
    hrtf._theta_min = 5.0 * M_PI / 6.0;
    // Undocumented for now, set to value shown.
    hrtf._trso_rflct = 0.3;

    // The `source_info` structure (of type `_source_information`, defined in `vsengine.h`) holds all the 
    // information about the sound source in the scene.

    // To understand the first four parameters, it is necessary to dig a bit into how the processing is done.
    // The full impulse response is split into two parts: "real-time", and "fixed-tail."

    // The fixed-tail part corresponds to the late reverberation and is *precomputed* when `_vsengine_setup_source()` 
    // is called. The real-time part is dynamically updated in accordance with current source / listener position(s)
    // stored in the `_source_information` struct each time a block of data is processed.

    // Finally, the whole impulse response is stored in a buffer of a fixed length. The response can in fact be 
    // shorter if the reverberation dies quickly, but cannot be longer; if it is longer, it is truncated.

    // The sound data is processed in chunks. The chunk size (in samples) is specified here, and when the source
    // is created it can NOT be changed afterwards.
    source_info._data_chunk_size = SAMPLES_PER_CHUNK;

    // The maximum order of reflections in the real-time, on-the-fly-recomputed part. 
    source_info._rir_realtime_order = 3;

    // The maximum order of reflections in the fixed-tail, precomputed part.
    source_info._rir_fixdtail_order = 32;

    // `_rir_length` is the length to use for the output buffer, in samples. To determine this, first compute the amount of clock
    // time for the response buffer (ie, the length of the expected longest reverberation, 0.74 seconds here),
    // then determine the number of chunks needed for that time (plus one), and then set 
    // `_rir_length` to the number of samples needed to store those chunks.
    double response_buffer_time_duration = 0.74;
    int num_response_chunks = (int)((double)response_buffer_time_duration * (double)CD_SAMPLES_PER_SECOND) / SAMPLES_PER_CHUNK + 1;
    source_info._rir_length = num_response_chunks * SAMPLES_PER_CHUNK;

    // If set to 0, HRTF interpolation is disabled. If set to 1, the HRTF interpolation is performed
    // (only for the direct sound arrival, not for any reflections, hence this flag is consulted only by `_vsengine_produce_odata()`).
    // The HRTF magnitude is interpolated using three nearest neighbors and HRTF phase is set using interaural time 
    // delay computed via HAT model. *Note that interpolation adds computational load, but without it in some scenarios 
    // and for some HRTF sets (especially sparsely sampled ones) audible jumps occur when the source is moved.*
    source_info._interpolate_hrtf = 1;

    // If set to 0, reverb randomization is disabled. If set to 1, the structure of the reverberant tail (specifically, 
    // the arrival time of each reflection) is slightly perturbed randomly so that the symmetry of the reverberation
    // field in a simulated room is broken, leading to less artifacts in rendering. It is recommended to set it to 1.
    source_info._randomize_reverb = 1;

    // The `_xrelativity_mode` parameter controls handling of positioning geometry. There are two supported relativity modes -- mode 0 and mode 2.
    // Depending on the mode chosen, head tracking information is processed differently. For gaming, mode 2 makes more sense and is recommended. 
    // See the SDK documentation for more info.
    source_info._xrelativity_mode = 2;

    // Sets the valid range (in meters) within which a listener can be from a source. If closer than `_source_range_min`,
    // the listener is assumed to be at `_source_range_min`. For `_source_range_max`, after the listener is farther than
    // half way between the source and `_source_range_max`, the sound has a linearly interpolated volume gain to reduce the volume.
    // Beyond `_source_range_max`, the source is no longer processed by the engine, a useful optimization. For this simple example,
    // source range clipping is effectively disabled by these values.
    source_info._source_range_min = 0;
    source_info._source_range_max = 3.08567758e+16;

    // Provides an alternative way to reduce excessive volume gain. Should be set to a similar value as
    // you would set `_source_range_min` but provides a smooth decay curve by performing a modified norm 
    // computation `sqrt(distance^2 + _distance_lqlimit^2)` for the distance. See the SDK documentation for more info.
    source_info._distance_lqlimit = 0.25;

    // Room size, in meters. (Defined in [constants.h](constants.html))
    source_info._roomxe_size[0] = ROOM_WIDTH;
    source_info._roomxe_size[1] = ROOM_LENGTH;
    source_info._roomxe_size[2] = ROOM_HEIGHT;

    // Listener and source position and orientation. All initialized to zero.
    // See notes under *Updating the Source and Listener* and the `update_vs_engine_*` functions below.
    for (int i = 0; i < 3; i++)
    {
        source_info._listnr_iypr[i] = 0.0;
        source_info._listnr_dxyz[i] = 0.0;
        source_info._listnr_dypr[i] = 0.0;
        source_info._offset_ixyz[i] = 0.0;
    }

    // `_rflctn_coef` is a two-dimensional array of wall reflection coefficients, specified per wall and per "channel."
    // Each element has values for `_VSENGINE_RFNBANDS` channels. (defined in `vsengine.h`). The concept of a channel is still under development,
    // and the provided coefficients must be identical for all channels. Each number in the array should be between 
    // zero and one.
    
    // The first index of the _rflctn_coef array is the wall number. The walls are numbered as shown. In this example,
    // the reflection coefficients are defined in [constants.h](constants.html) and passed in here.
    for (int i = 0; i < _VSENGINE_RFNBANDS; i++) {
        source_info._rflctn_coef[0][i] = LEFT_WALL_REFLECTION;
        source_info._rflctn_coef[1][i] = RIGHT_WALL_REFLECTION;
        source_info._rflctn_coef[2][i] = FRONT_WALL_REFLECTION;
        source_info._rflctn_coef[3][i] = BACK_WALL_REFLECTION;
        source_info._rflctn_coef[4][i] = CEILING_WALL_REFLECTION;
        source_info._rflctn_coef[5][i] = FLOOR_WALL_REFLECTION;
    }

    // `_rfomlf_coef` contains reflection dampening coefficients, early reflections of order N are multiplied by the Nth element
    // of this `_VSENGINE_MAXRFORDER` sized array. Set all elements to one to disable dampening.
    for (int i = 0; i < _VSENGINE_MAXRFORDER; i++) {
        source_info._rfomlf_coef[i] = 1.0;
    }

    // Undocumented for now, set to specified value.
    source_info._ffdirection_enbl = 0;
    // Undocumented for now, set to specified value.
    source_info._ffdirection_angl = 0;
    // Undocumented for now, set to specified value.
    source_info._rshape_polyhedral = NULL;

    // Make copy of initial source configuration for moving source interpolation, see `perform_spatial_audio_transform()`.
    source_info_previous = source_info;

    return(0);
}

// **init_vs_engine** performs the necessary steps to prepare the engine for audio processing. These
// steps include initializing the HRTF and initializing the source given the configuration defined 
// in `setup_vsengine_config()`.
int init_vsengine(void)
{
    // Create the mutex needed for the critical block in `perform_spatial_audio_transform()`.
    pthread_mutex_init(&position_mutex, NULL);

    // Load the source audio data and # of samples from `SOURCE_FILENAME`. (Defined in [constants.h](constants.html))
    load_source_audio_data();

    // Intiailize the current sample index to zero, so playback of the source begins from the beginning of the audio stream.
    current_source_sample_index = 0;

    // Generate the full license key string. The license key is required and checked during HRTF preparation.
    char *license_key = (char *)malloc(strlen(VS_KEY_TEXT) + 128);
    sprintf(license_key, "%s-%d-%d-%d-%d", VS_KEY_TEXT, VS_KEY_NUMBER1, VS_KEY_NUMBER2, VS_KEY_NUMBER3, VS_KEY_NUMBER4);

    // The first step needed to use the audio engine is to prepare the HRTF using `_vsengine_prepare_hrtf()`. 

    // In this example the raw HRTF samples are in `_hrtf_data_points`, a statically allocated array `#included` via `hrtf-cp048-v003.h`.
    // To read the HRTF from disk, pass a filename as the first argument. Otherwise, to read from memory, as is done here,
    // pass the data pointer and the data length as the 4th and 5th arguments. 

    // In addition to the HRTF data, the HRTF data structure (`hrtf`, set up in `vsengine_setup_config()`), output
    // sample rate (`CD_SAMPLES_PER_SECOND`, 44khz) and license key are passed as well. After the call, the `hrtf`
    // structure will be populated with HRTF data.

    // If the playback frequency differs from HRTF sampling frequency, the HRTF set will be resampled on loading (may take up to a minute.)
    // You may have multiple HRTF sets loaded and use different HRTF sets to process different sources. 
    int error = _vsengine_prepare_hrtf(NULL, &hrtf, CD_SAMPLES_PER_SECOND, _hrtf_data_points, _HDATA_LEN, license_key);
    free(license_key);

    if (error)
    {
        printf("_vsengine_prepare_hrtf failed, error code %d\n", error);
        return(1);
    }

    // Once the HRTF is loaded the `hrtf_hash` auxiliary data structure is populated via `_vsengine_prepare_hrtf_hash()`.
    // This *optional* data structure (one per source) can provide a small performance speedup if passed to other RealSpace3D APIs.
    error = _vsengine_prepare_hrtf_hash(&source_info, &hrtf, &hrtf_hash);

    if (error)
    {
        printf("_vsengine_prepare_hrtf_hash failed, error code %d\n", error);
        return(1);
    }

    // For each sound source a "work area" scratch space needs to be created via `_vsengine_prepare_work_area()`. In this example
    // there is only one sound source but with multiple sources you would call this function once each time, passing in the
    // proper `_source_information` structure for each sound source. After the call here, pointers to the allocated scratch memory 
    // will be set in the `source_work_area` structure.

    // The third parameter is an optimization level ranging from 
    // 0-3. The higher is the number, the longer it will take `_vsengine_prepare_work_area()` to complete and the faster the 
    // engine will operate when processing the actual data. In practice, going above one is very unlikely to give 
    // any additional speed-up.
    // On Android, set optimization level to zero. Otherwise, the _prepare_work_area call takes excessive time.
    error = _vsengine_prepare_work_area(&source_info, &source_work_area, 1);

    if (error)
    {
        printf("_vsengine_prepare_work_area failed, error code %d\n", error);
        return(1);
    }

    // Finally, `_vsengine_setup_source()` is used to precompute the fixed-tail contribution for the source described in `source_info`.
    // Again, for multiple sources this must be called for each source. The fixed-tail reflections are generally the same
    // for a given room configuration, regardless of listener or source position. However, if the room configuration
    // is changed (in the `_source_information` structure) then `_vsengine_setup_source()` should be called again.
    // After the call, the fixed-tail will be stored in the `source_work_area` scratch area.
    error = _vsengine_setup_source(&source_info, &source_work_area, &hrtf);

    if (error)
    {
        printf("_vsengine_setup_source failed, error code %d\n", error);
        return(1);
    }

    return 0;
}

// Note on RealSpace3D coordinate system
// -------------------------------------
// In the sound engine, +X goes from center of listener's head to right ear, -Y (yes, *negative*) goes from listener's
// head center to listener's nose, and +Z goes from listener's head center *down*.

// The origin is defined to be the center of the room. So a room with width 4.7244 has valid range of X as (-2.3622, 2.3622). 

// In this example, the origin and units of OpenGL scene space and RealSpace3D engine space are the same, but the orientation and labels of the 
// axes are not, as reflected in the conversions done below.

// Updating the source and listener
// --------------------------------
// The functions `update_vs_engine_source_position()`, `update_vsengine_listener_position()`,
// and `update_vsengine_listener_orientation()` are called after the user moves the camera or the
// source in the scene. 

// In these functions, the `source_info` data structure is updated in a critical block in order to
// make the sound engine process subsequent audio blocks in a way that reflects the updated scene. (The critical block
// is necessary in order to avoid a situation where the `source_info` data structure changes *while* the 
// engine is working on performing an audio transformation.)

// In principle, once you set up the source, you can change all other `source_info` members in the sense 
// that the engine will not return an error. However, you need to remember that the reverberation tail 
// (i.e., all reflections of order higher than `_rir_realtime_order`) is computed at the time of the `_vsengine_setup_source()` call.
// Hence, if you provide parameters P1 to `_vsengine_setup_source()` and P2 later to `_vsengine_produce_odata()`, 
// the "early reflections" part of the output would use set P2 and the rest would still reflect the settings from P1.
// This is likely to sound weird and / or create an impression that the new parameters did not take effect.
// To reinterate: there are only a few things that you should change on the fly - the things that do 
// *not* affect the reverberation tail. These are: source position, listener position / orientation, and 
// speaker orientation. (The first two of which are demonstrated here.) If you change anything else, you should call 
// `_vsengine_setup_source()` again.

// **update_vs_engine_source_position** sets the source position in RealSpace3D engine space given coordinates in
// OpenGL world space. Note the flipping of the axes and the mapping of Y to Z and vice versa.
void update_vsengine_source_position(float x, float y, float z)
{
    pthread_mutex_lock(&position_mutex);
    source_info._source_ixyz[0] = -1.0 * x;
    source_info._source_ixyz[1] = -1.0 * z;
    source_info._source_ixyz[2] = -1.0 * y;
    pthread_mutex_unlock(&position_mutex);
}

// **update_vs_engine_listener_orientation** sets the listener orientation via conversion to Yaw and Roll in 
// RealSpace 3D space.
void update_vsengine_listener_orientation(float psi, float theta)
{
    pthread_mutex_lock(&position_mutex);
    source_info._listnr_dypr[0] = -1.0 * M_PI * psi / 180.0;
    source_info._listnr_dypr[1] = 0.0;
    source_info._listnr_dypr[2] = 1.0 * M_PI * theta / 180.0;
    pthread_mutex_unlock(&position_mutex);
}

// **update_vs_engine_listener_position** sets the listener position in RealSpace3D engine space given coordinates in
// OpenGL world space. Note the flipping of the axes and the mapping of Y to Z and vice versa.
void update_vsengine_listener_position(float x, float y, float z)
{
    pthread_mutex_lock(&position_mutex);
    source_info._listnr_ixyz[0] = -1.0 * x;
    source_info._listnr_ixyz[1] = -1.0 * z;
    source_info._listnr_ixyz[2] = -1.0 * y;
    pthread_mutex_unlock(&position_mutex);
}

// **perform_spatial_audio_transform** uses the RealSpace3D engine to transform the next chunk of the (mono) sound source
// audio stream into stereo 3D audio output stream. The next chunk to read is determined via the `current_source_sample_index`
// global variable. The parameter `out` is the pointer to write `SAMPLES_PER_CHUNK` transformed 16-bit integer 
// stereo samples into.

// In this example, only a single source source exists in the scene. To mix outputs from multiple sources, just add
// them up.
void perform_spatial_audio_transform(short *out)
{
    // The input data block must be *twice* the chunk size, and must be filled fully with data. This is necessary
    // to avoid boundary artifacts (ie clicks) on playback.

    // `vs_idata` will contain the input block and is allocated to contain 2 chunks of samples, starting at `current_source_sample_index`,
    // and the samples are copied from `source_data`. In this example the sample is looped, so if the index
    // spills past the end of the sample the modulus operator wraps it to the beginning.
    float *vs_idata = (float *)malloc(2 * SAMPLES_PER_CHUNK * sizeof(float));

    memset(out, 0, 2 * SAMPLES_PER_CHUNK * sizeof(short));

    for (int i_sample = 0; i_sample < 2 * SAMPLES_PER_CHUNK; i_sample++)  {
        int source_sample_index = current_source_sample_index + i_sample;
        vs_idata[i_sample] = source_data[source_sample_index % source_length];
    }

    // In a moving listener or source scenario, such as the one in this example, rapid HRTF changes can produce
    // artifacts. To reduce these artifacts, a copy of the previous source/listener configuration is kept in 
    // `source_info_previous`, and the input block is transformed with both the current and previous
    // configuration. Then, these two streams are interpolated over time in order to reduce artifacts (see below.) 

    // `vs_odataA_*` will contain the transformed samples for the left and right channels using the *previous*
    // source and listener positions, and `vs_odataB_*` will contain the same using the *current* source and
    // listener positions. Note the output buffers, unlike the input buffer, only contain a single chunk's worth of
    // samples.
    float *vs_odataA_left = (float *)malloc( SAMPLES_PER_CHUNK * sizeof(float));
    float *vs_odataA_right = (float *)malloc( SAMPLES_PER_CHUNK * sizeof(float));
    float *vs_odataB_left = (float *)malloc( SAMPLES_PER_CHUNK * sizeof(float));
    float *vs_odataB_right = (float *)malloc( SAMPLES_PER_CHUNK * sizeof(float));

    // Enter the critical section. This mutex ensures that *no* updates to source or listener position occur
    // while the engine is processing audio data. (The corresponding critical sections can be found in
    // the `update_listener_*` and `update_source_*` functions to move the listener and source position.)
    pthread_mutex_lock(&position_mutex);

    // To feed the input data block into the engine for a source call `_vsengine_submit_idata()`. Again note that the `vs_idata`
    // buffer contains *twice* the chunk size number of samples. Beyond this, the SDK maintains a queue of past
    // data (in order to be able to compute late reverberation.) Upon completion, this function will discard the 
    // least recent data block, push all blocks in the queue one block back, and insert the new data block 
    // into the queue. This function copies data from input buffer; you may free and/or reuse the input buffer 
    // space immediately after this function has been returned.
    int error = _vsengine_submit_idata(&source_info, &source_work_area, vs_idata);
    if (error)
    {
        printf("VSENGINE submit data failed, error code %d\n", error);
        audio_thread_enabled = 0;
        return;
    }

    // Next, `_vsengine_produce_support()` computes auxiliary data required for the production of output data.
    // Once this call is complete, you can make multiple calls to `_vsengine_produce_odata()` for the same source
    // and listener *at different positions*, as is done here for the interpolation step done below. Since some 
    // steps for producing output at different positions are reptitive, `_vsengine_produce_support()` has been 
    // factored out for efficiency.
    error = _vsengine_produce_support(&source_info, &source_work_area);
    if (error)
    {
        printf("VSENGINE produce support failed, error code %d\n", error);
        audio_thread_enabled = 0;
        return;
    }

    // Finally, `_vsengine_produce_odata()` is called, which generates the transformed spatial audio samples for
    // both the left and right channels. It is called twice, once using the `source_info_previous` source and listener
    // positions, and again using the current `source_info` source and listener positions. 
    error = _vsengine_produce_odata(&source_info_previous, &source_work_area, &hrtf, &hrtf_hash, vs_odataA_left, vs_odataA_right);
    if (error)
    {
        printf("VSENGINE produce odata (series A) failed, error code %d\n", error);
        audio_thread_enabled = 0;
        return;
    }

    error = _vsengine_produce_odata(&source_info, &source_work_area, &hrtf, &hrtf_hash, vs_odataB_left, vs_odataB_right);
    if (error)
    {
        printf("VSENGINE produce odata (series B) failed, error code %d\n", error);
        audio_thread_enabled = 0;
        return;
    }

    // Now that the transformation is finished, update `source_info_previous` to reflect the current `source_info`.
    source_info_previous = source_info;

    // End critical section.
    pthread_mutex_unlock(&position_mutex);

    // To reduce artifacts in the case of moving sources or listeners, the two transformed streams are interpolated.
    // As noted above, `vs_odataA_*` contains the transformed samples for the left and right channels using the 
    // *previous* source and listener positions, and `vs_odataB_*` contains the same using the *current* source
    // and listener positions.

    // Note that we *always* perform this interpolation, but it is not strictly necessary unless `source_info`
    // indicates that the source or listener position has changed relative to `source_info_previous` (ie, the
    // user has pressed a motion key.)

    for (int i_sample = 0; i_sample < SAMPLES_PER_CHUNK; i_sample++) {
        // The interpolation mixes the first `SERIES_A_SAMPLES` (defined in [constants.h](constants.html)) of the two streams,
        // dampening down the contribution  of the `vs_odataA_*` stream linearly to zero. We also apply a constant
        // `PLAYBACK_GAIN` volume gain.
        float series_a_weight = (i_sample <= SERIES_A_SAMPLES ? 1.0f - (float)i_sample / (float)SERIES_A_SAMPLES : 0.00f);
        float series_b_weight = 1.0f - series_a_weight;

        float left = PLAYBACK_GAIN * series_a_weight * vs_odataA_left[i_sample] + PLAYBACK_GAIN * series_b_weight * vs_odataB_left[i_sample];
        float right = PLAYBACK_GAIN * series_a_weight * vs_odataA_right[i_sample] + PLAYBACK_GAIN * series_b_weight * vs_odataB_right[i_sample];

        // Finally, the output buffer is populated with the final transformed & clipped interleaved stereo 16-bit samples.
        out[2 * i_sample + 0] += (short)(32768.0f * fmaxf(fminf(left, MAX_SAMPLE_VOLUME), MIN_SAMPLE_VOLUME));
        out[2 * i_sample + 1] += (short)(32768.0f * fmaxf(fminf(right, MAX_SAMPLE_VOLUME), MIN_SAMPLE_VOLUME));
    }

    // Update the global sample index counter, and wrap so the sample loops.
    current_source_sample_index += SAMPLES_PER_CHUNK;

    if (current_source_sample_index >= source_length)
    {
        current_source_sample_index = 0;
    }
    
    // Free memory, the only output of this function is the `out` buffer of stereo 16-bit transformed samples.
    free(vs_idata);
    free(vs_odataA_left);
    free(vs_odataA_right);
    free(vs_odataB_left);
    free(vs_odataB_right);

    return;
}

// **audio_thread_exec** is the entry point for the audio playback thread. It executes the audio processing
// loop, where each chunk of audio is processed by the RealSpace3D sound engine and sent to the audio device
// for playback. 
void* audio_thread_exec(void *_xdata)
{
    // Attach pthreads.
    pthread_win32_thread_attach_np();

    // The playback loop uses a "frame buffer" approach where while one chunk of audio has been sent to the device
    // the next chunk of audio may be being transformed and prepared for playback. So, two playback buffers are set
    // up and a toggle flag between the two is used.

    // Allocate the two playback sample buffers to contain one chunk of stereo 16-bit samples.
    short* playback_buffer_1 = (short *)malloc(2 * SAMPLES_PER_CHUNK * sizeof(short));
    if (!playback_buffer_1)
    {
        puts("malloc failed");
        audio_thread_enabled = 0;
        return NULL;
    }
    short *playback_buffer_2 = (short *)malloc(2 * SAMPLES_PER_CHUNK * sizeof(short));
    if (!playback_buffer_2)
    {
        puts("malloc failed");
        audio_thread_enabled = 0;
        return NULL;
    }

    // Setup the WAV output specification needed to use the win32 audio APIs. Two channel, 16-bit audio is emitted
    // by the `perform_spatial_audio_transform()` function, so that is what is set here.
    WAVEFORMATEX wave_format;
    wave_format.wFormatTag = WAVE_FORMAT_PCM;
    wave_format.nChannels = 2;
    wave_format.nSamplesPerSec = CD_SAMPLES_PER_SECOND;
    wave_format.nAvgBytesPerSec = 2 * CD_SAMPLES_PER_SECOND * sizeof(short);
    wave_format.nBlockAlign = 2 * sizeof(short);
    wave_format.wBitsPerSample = sizeof(short) * 8;
    wave_format.cbSize = 0;

    // Create an event which will be waited on until the system is ready for the next chunk of audio.
    HANDLE done_event = CreateEvent(NULL, FALSE, FALSE, NULL);

    // Open the default playback device.
    HWAVEOUT wave_out_device;
    int error = waveOutOpen(&wave_out_device, WAVE_MAPPER, &wave_format, (DWORD_PTR)done_event, 0, CALLBACK_EVENT );

    if (error)
    {
        puts("waveOutOpen failed");
        audio_thread_enabled = 0;
        return NULL;
    }

    // Perform the 3d audio transform for the first two chunks of audio via (`perform_spatial_audio_transform()`, defined above)
    // and write them to the device. 
    perform_spatial_audio_transform(playback_buffer_1);
    WAVEHDR wave_header_1;
    wave_header_1.lpData = (char *)(playback_buffer_1);
    wave_header_1.dwBufferLength = 2 * SAMPLES_PER_CHUNK * sizeof(short);
    wave_header_1.dwFlags = 0;
    waveOutPrepareHeader(wave_out_device, &wave_header_1, sizeof(wave_header_1));
    waveOutWrite(wave_out_device, &wave_header_1, sizeof(wave_header_1));

    perform_spatial_audio_transform(playback_buffer_2);
    WAVEHDR wave_header_2;
    wave_header_2.lpData = (char *)(playback_buffer_2);
    wave_header_2.dwBufferLength = 2 * SAMPLES_PER_CHUNK * sizeof(short);
    wave_header_2.dwFlags = 0;

    waveOutPrepareHeader(wave_out_device, &wave_header_2, sizeof(wave_header_2));
    waveOutWrite(wave_out_device, &wave_header_2, sizeof(wave_header_2));

    // Set simple toggle flag used to alternate working sound buffer.
    int flip_flop = 1;

    // The current buffer and WAV header specification are referenced via these two pointers.
    WAVEHDR* current_wave_header;
    short** current_playback_buffer;

    // Continue playback until shutdown is signaled via `audio_thread_enabled` being set to false.
    while (audio_thread_enabled)
    {
        // Wait until device is ready for more audio.
        ResetEvent(done_event);
        WaitForSingleObject(done_event, INFINITE );

        // Set the pointers based upon the buffer flag.
        if (flip_flop)
        {
            current_wave_header = &wave_header_1;
            current_playback_buffer = &playback_buffer_1;
        }
        else
        {
            current_wave_header = &wave_header_2;
            current_playback_buffer = &playback_buffer_2;
        }

        // Check flags on the WAV header to ensure that the event was triggered because the device was ready for more audio.
        if ((current_wave_header->dwFlags & WHDR_DONE) == 0) {
            puts("*** _audio_output_thread: WARNING: buf completion not rcvd as expected");
        }

        // Perform the audio transform for the next chunk of samples, reset the flags, and write it to the proper output buffer.
        waveOutUnprepareHeader(wave_out_device, current_wave_header, sizeof(WAVEHDR));
        perform_spatial_audio_transform(*current_playback_buffer);
        current_wave_header->lpData = (char *)(*current_playback_buffer);
        current_wave_header->dwBufferLength = 2 * SAMPLES_PER_CHUNK * sizeof(short);
        current_wave_header->dwFlags = 0;
        waveOutPrepareHeader(wave_out_device, current_wave_header, sizeof(WAVEHDR));
        waveOutWrite(wave_out_device, current_wave_header, sizeof(WAVEHDR));

        // Toggle the buffer flag.
        flip_flop ^= 1;
    }

    // Clean up resources.
    waveOutReset(wave_out_device);
    waveOutClose(wave_out_device);
    CloseHandle(done_event);
    pthread_win32_thread_detach_np();
    return(NULL);
}

// **shutdown_audio_thread** flips off the flag and breaks out of the main playback loop in `audio_thread_exec`.
void shutdown_audio_thread()
{
    audio_thread_enabled = 0;
}

// **cleanup_audio_thread** frees resources needed during audio playback.
void cleanup_audio_thread()
{
    // `_vsengine_cleanup_work_area` frees resources for work areas allocated via `_vsengine_prepare_work_area()`.
    int error = _vsengine_cleanup_work_area(&source_info, &source_work_area);
    if (error)
    {
        printf("_vsengine_cleanup_work_area failed, error code %d\n", error);
    }

    // `_vsengine_cleanup_hrtf` frees resources for HRTFs prepared via `_vsengine_prepare_hrtf()`.
    error = _vsengine_cleanup_hrtf(&hrtf);
    if (error)
    {
        printf("_vsengine_cleanup_hrtf failed, error code %d\n", error);
    }

    // Destroy the mutex used for the critical section.
    pthread_mutex_destroy(&position_mutex);
}