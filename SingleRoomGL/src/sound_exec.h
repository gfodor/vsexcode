#ifndef VS_SOUND_EXEC_H
#define VS_SOUND_EXEC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <windows.h>

#include "vsengine.h"

#include "constants.h"
#include "sound_load.h"

#define	PTW32_STATIC_LIB

#include "pthread.h"

int setup_vsengine_config();
void shutdown_audio_thread();
void cleanup_audio_thread();
int init_vsengine();
void* audio_thread_exec(void *_xdata);

void update_vsengine_source_position(float x, float y, float z);
void update_vsengine_listener_position(float x, float y, float z);
void update_vsengine_listener_orientation(float psi, float theta);

#endif