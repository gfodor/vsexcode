#ifndef VS_SOUND_LOAD_H
#define VS_SOUND_LOAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <windows.h>
#include "libresample.h"
#include "libmpg123.h"
#include "constants.h"

int read_sound_file_data(char *_fname, int _channel, float *_data, int _target_frequency);
int read_sound_file_length(char *_fname, int _channel, int _target_frequency);

struct _mp3dta
{
    float *_data;
    int _scnt;
    struct _mp3dta *_next;
};

#ifdef __cplusplus
extern "C"
#endif
// -------------------------------------------------------------
int _m_round(double);

#endif