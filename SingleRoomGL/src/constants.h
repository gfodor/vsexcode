// The `constants.h` file defines constants used throughout this example.
#ifndef VS_CONSTANTS_H
#define VS_CONSTANTS_H

#define	M_E	2.718281828459045235400
#define	M_PI 3.141592653589793238460

// CD samples per second, used as the normalized sample rate for all audio data.
#define	CD_SAMPLES_PER_SECOND	44100

// Room width and height in meters, see `setup_vsengine_config()` in [sound_exec.c](sound_exec.html).
#define ROOM_WIDTH 5.88f
#define ROOM_HEIGHT 4.20f 
#define ROOM_LENGTH 7.98f

// Room wall reflectivity constants, see `setup_vsengine_config()` in [sound_exec.c](sound_exec.html).
#define LEFT_WALL_REFLECTION 0.85f 
#define RIGHT_WALL_REFLECTION 0.85f
#define FRONT_WALL_REFLECTION 0.75f
#define BACK_WALL_REFLECTION 0.95f
#define CEILING_WALL_REFLECTION 0.55f
#define FLOOR_WALL_REFLECTION 0.65f

// License key information.
#define	VS_KEY_TEXT	"Oculus-Test-License-Valid-Through-June-2014-Key"
#define	VS_KEY_NUMBER1 22181
#define	VS_KEY_NUMBER2 30748
#define	VS_KEY_NUMBER3 31504
#define	VS_KEY_NUMBER4 49619

// Number of samples to interpolate between previous position audio and current position audio chunk, 
// see `perform_spatial_audio_transform()` in [sound_exec.c](sound_exec.html).
#define	SERIES_A_SAMPLES 127

// Samples per "chunk", the unit of audio processing used by the audio processing thread.
#define	SAMPLES_PER_CHUNK 4096

// Configurable gain on the output audio stream.
#define PLAYBACK_GAIN 0.20f

// Minimum distance the user is allowed to move the camera from the wall. The user will be blocked from getting closer.
#define	MINIMUM_WALL_DISTANCE 0.250f

// Valid range for sample values.
#define	MIN_SAMPLE_VOLUME -0.995f
#define	MAX_SAMPLE_VOLUME 0.995f

// Initial player height, used for setting initial camera position.
#define PLAYER_HEIGHT 1.72f;

// Filename to load for audio to use for the sound source in the scene.
#define SOURCE_FILENAME "example.mp3"

#endif