/* This is the header file for VisiSonics Spatial Engine DLL (3.0.20140416) */

#define	_VSENGINE_NO_ERROR		0

#define	_VSENGINE_BASE_ERROR_NUMBER	-16639

#define	_VSENGINE_INTERNAL_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  1)
#define	_VSENGINE_INVALID_PARAMETER	(_VSENGINE_BASE_ERROR_NUMBER -  2)
#define	_VSENGINE_MALLOC_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  3)
#define	_VSENGINE_FFTW_PLAN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  4)
#define	_VSENGINE_HRTF_OPEN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  5)
#define	_VSENGINE_HRTF_READ_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  6)
#define	_VSENGINE_HRTF_HECT_EXCESS	(_VSENGINE_BASE_ERROR_NUMBER -  7)
#define	_VSENGINE_INIT_REQUIRED		(_VSENGINE_BASE_ERROR_NUMBER -  8)
#define	_VSENGINE_ILLEGAL_UPDATE	(_VSENGINE_BASE_ERROR_NUMBER -  9)
#define	_VSENGINE_SRCC_OUT_OF_RANGE	(_VSENGINE_BASE_ERROR_NUMBER - 10)
#define	_VSENGINE_HASH_UNAVAILABLE	(_VSENGINE_BASE_ERROR_NUMBER - 11)
#define	_VSENGINE_INVALID_POLYGON	(_VSENGINE_BASE_ERROR_NUMBER - 12)
#define	_VSENGINE_INVALID_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 13)
#define	_VSENGINE_INVALID_PLATFORM	(_VSENGINE_BASE_ERROR_NUMBER - 14)
#define	_VSENGINE_NACTIVE_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 15)
#define	_VSENGINE_EXPIRED_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 16)

#define	_VSENGINE_RFNBANDS		48
#define	_VSENGINE_MAXRFORDER		128

struct _vecpnt { double _x[3]; int _pv; int _type; };

struct _bpoint { double _x, _y, _z; struct _bpoint *_p_next; };

struct _rflist { struct _bbface *_rfpntr; struct _rflist *_r_next; };

struct _bbface {
	int	_obstructive;
	struct	_bpoint *_fnrm; double _fdxo;
	struct	_bpoint *_p_list;
	struct	_bbface *_f_next;
	double	_attenuation[_VSENGINE_RFNBANDS]; };

struct _bbvsrc {
	int	_level, _vsb_ok;
	struct	_bpoint *_vspnt;
	struct	_bbface *_rface;
	struct	_bbvsrc *_sibling;
	struct	_bbvsrc *_children;
	struct	_bbvsrc *_parent;
	struct	_rflist *_flctrs; };

struct _hrtf_data { int _internal_data_0[5];
	int	*_internal_data_1[5];
	double	_head_radius;
	double	_trso_radius;
	double	_neck_height;
	double	_alpha_min;
	double	_theta_min;
	double	_trso_rflct; };

struct _source_information {
	int		_data_chunk_size;
	int		_rir_realtime_order;
	int		_rir_fixdtail_order;
	int		_rir_length;
	int		_interpolate_hrtf;
	int		_randomize_reverb;
	int		_xrelativity_mode;
	int		_ffdirection_enbl;
	double		_ffdirection_angl;
	double		_distance_lqlimit;
	double		_source_range_min;
	double		_source_range_max;
	int		_source_range_asp;
	int		_source_decay_cut;
	double	_roomxe_size[3];
	double	_source_ixyz[3];
	double	_listnr_ixyz[3];
	double	_listnr_iypr[3];
	double	_listnr_dxyz[3];
	double	_listnr_dypr[3];
	double	_offset_ixyz[3];
	double	_rflctn_coef[6][_VSENGINE_RFNBANDS];
	double	_rfomlf_coef   [_VSENGINE_MAXRFORDER];
	struct	_bbface *_rshape_polyhedral; };

struct _source_work_area { unsigned char _internal_data[240]; };

struct _hrtf_hash { unsigned char _internal_data[24]; };

// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_get_version(int *_major_version,
	int *_minor_version, int *_build_date);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_verify_hrtf_data(struct _hrtf_data *_h0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_verify_hrtf_hash(struct _hrtf_hash *_a0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_verify_work_area(struct _source_work_area *_w0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_prepare_hrtf(const char *_filename,
	struct _hrtf_data *_h0, int _sampling_frequency,
	unsigned char *_hrtf_fbcontent, int _hrtf_fbclen,
	const unsigned char *_license_key);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_prepare_work_area(struct _source_information *_s0,
	struct _source_work_area *_w0, int _effort_level);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_assign_normal(struct _bbface *_face);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_setup_source(struct _source_information *_s0,
	struct _source_work_area *_w0, struct _hrtf_data *_h0);
// -------------------------------------------------------------



// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_prepare_hrtf_hash(struct _source_information *_s0,
	struct _hrtf_data *_h0, struct _hrtf_hash *_a0);
// -------------------------------------------------------------



// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_flush_source(struct _source_information *_s0,
	struct _source_work_area *_w0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_submit_idata(struct _source_information *_s0,
	struct _source_work_area *_w0, float *_inp_block);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_produce_support(struct _source_information *_s0,
	struct _source_work_area *_w0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_produce_odata(struct _source_information *_s0,
	struct _source_work_area *_w0, struct _hrtf_data *_h0,
	struct _hrtf_hash *_a0,
	float *_out_block_left, float *_out_block_rght);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_cleanup_work_area(struct _source_information *_s0,
	struct _source_work_area *_w0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_cleanup_hrtf_hash(struct _hrtf_hash *_a0);
// -------------------------------------------------------------


// -------------------------------------------------------------
// Description Here
// -------------------------------------------------------------
#ifdef _VSENGINE_DLL_BUILD_
	__declspec(dllexport)
#endif
// -------------------------------------------------------------
#ifdef __cplusplus
	extern "C"
#endif
// -------------------------------------------------------------
int _vsengine_cleanup_hrtf(struct _hrtf_data *_h0);
// -------------------------------------------------------------
