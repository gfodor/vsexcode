// The `sound_load.c` file defines functions used to load and resample WAV and MP3 files to be used.
// by the RealSpace3D engine. It is left largely undocumented, since implementing these functions is largely
// irrelevant to using the 3D audio engine. 
#include "sound_load.h"

// **_verify_wav_guid** checks that a guid is a valid WAV file guid.
int _verify_wav_guid(char *_guid)
{ /* returns 1 if guid is correct */

    if (_guid[0x00] == (char)0x01); else return(0);
    if (_guid[0x01] == (char)0x00); else return(0);
    if (_guid[0x02] == (char)0x00); else return(0);
    if (_guid[0x03] == (char)0x00); else return(0);
    if (_guid[0x04] == (char)0x00); else return(0);
    if (_guid[0x05] == (char)0x00); else return(0);
    if (_guid[0x06] == (char)0x10); else return(0);
    if (_guid[0x07] == (char)0x00); else return(0);
    if (_guid[0x08] == (char)0x80); else return(0);
    if (_guid[0x09] == (char)0x00); else return(0);
    if (_guid[0x0A] == (char)0x00); else return(0);
    if (_guid[0x0B] == (char)0xAA); else return(0);
    if (_guid[0x0C] == (char)0x00); else return(0);
    if (_guid[0x0D] == (char)0x38); else return(0);
    if (_guid[0x0E] == (char)0x9B); else return(0);
    if (_guid[0x0F] == (char)0x71); else return(0);

    return(1);
}

// **_wave_file_length_inquiry** determines the number of samples in the specified WAV file if sampled at `_target_frequency`.
int _wave_file_length_inquiry(char *_fname, int _channel, int _target_frequency)
{
    int _hdl, _d;
    char *_read_buff = NULL, *_file_data = NULL;
    char _rbf[16];
    char _wav_guid[16];

    int _riff_chunk_size, _chunk_length, _keep_reading = 1, _fmt_parsed = 0;

    int _format_tag, _n_channels, _sampling_r, _bytes_scnd, _block_algn, _bits_smple, _extnsn_len, _data_chunk_length;

    _hdl = _open(_fname, _O_RDONLY | _O_BINARY );
    if (_hdl == -1)
    {
        puts("*** wav-linq: fatal error (code L00)");
        return(-1);
    }

    _d = _read(_hdl, _rbf, 0x0C);
    if (_d == 0x0C); else
    {
        puts("*** wav-linq: fatal error (code L01)");
        return(-1);
    }

    /* this should have RIFF <RIFF chunk size> WAVE */

    if (_rbf[0x00] == 'R' && _rbf[0x01] == 'I' && _rbf[0x02] == 'F' && _rbf[0x03] == 'F'); else
    {
        puts("*** wav-linq: fatal error (code L02)");
        return(-1);
    }

    _riff_chunk_size = *(int *)(_rbf + 0x04); /* what shall we do with the drunken sailor?... */

    if (_rbf[0x08] == 'W' && _rbf[0x09] == 'A' && _rbf[0x0A] == 'V' && _rbf[0x0B] == 'E'); else
    {
        puts("*** wav-linq: fatal error (code L03)");
        return(-1);
    }

    /* now we need to read chunks until we find the "data" chunk */
    /* if/when we meet "fmt " chunk, we need to parse it */

    while (_keep_reading)
    {
        _d = _read(_hdl, _rbf, 0x08);
        if (_d == 0x08); else
        {
            puts("*** wav-linq: fatal error (code L04)");
            return(-1);
        }

        _chunk_length = *(int *)(_rbf + 0x04);

        _read_buff = (unsigned char *)malloc(_chunk_length);

        if (_read_buff); else
        {
            puts("*** wav-linq: fatal error (code L05)");
            return(-1);
        }

        _d = _read(_hdl, _read_buff, _chunk_length);
        if (_d == _chunk_length); else
        {
            puts("*** wav-linq: fatal error (code L06)");
            return(-1);
        }

        if (_rbf[0x00] == 'f' && _rbf[0x01] == 'm' && _rbf[0x02] == 't' && _rbf[0x03] == ' ')
        {
            if (_chunk_length < 0x10)
            {
                puts("*** wav-linq: fatal error (code L07)");
                return(-1);
            }

            _format_tag = *(short *)(_read_buff + 0x00);
            _n_channels = *(short *)(_read_buff + 0x02);
            _sampling_r = *(int *)(_read_buff + 0x04);
            _bytes_scnd = *(int *)(_read_buff + 0x08);
            _block_algn = *(short *)(_read_buff + 0x0C);
            _bits_smple = *(short *)(_read_buff + 0x0E);

            if (_format_tag == -2)
                if (_chunk_length < 0x28)
                {
                    puts("*** wav-linq: fatal error (code L07a)");
                    return(-1);
                }
            if (_format_tag == -2) _extnsn_len = *(short *)(_read_buff + 0x10);
            if (_format_tag == -2)
                if (_extnsn_len < 0x16)
                {
                    puts("*** wav-linq: fatal error (code L07b)");
                    return(-1);
                }
            if (_format_tag == -2) memcpy(_wav_guid, _read_buff + 0x18, 0x10);

            _fmt_parsed = 1;
        }

        if (_rbf[0x00] == 'd' && _rbf[0x01] == 'a' && _rbf[0x02] == 't' && _rbf[0x03] == 'a')
            if (_fmt_parsed)
            {
                _data_chunk_length = _chunk_length;
                _keep_reading = 0;
                _file_data = (char *)malloc(_data_chunk_length);
                if (_file_data); else
                {
                    puts("*** wav-linq: fatal error (code L08)");
                    return(-1);
                }
                memcpy(_file_data, _read_buff, _chunk_length);
            }
            else
            {
                puts("*** wav-linq: fatal error (code L09)");
                return(-1);
            }

        free(_read_buff);
    }
    _close(_hdl);

    if (_fmt_parsed); else
    {
        puts("*** wav-linq: fatal error (code L10)");
        return(-1);
    }

    if (_format_tag == 1 || _format_tag == 3 || _format_tag == -2); else
    {
        puts("*** wav-linq: fatal error (code L11)");
        return(-1);
    }

    if (_format_tag == -2)
        if (!_verify_wav_guid(_wav_guid))
        {
            puts("*** wav-linq: fatal error (code L11a)");
            return(-1);
        }

    if (_format_tag == 3)
        if (_bits_smple == 32); else
        {
            puts("*** wav-linq: fatal error (code L11c)");
            return(-1);
        }

    if (0 <= _channel && _channel < _n_channels); else
    {
        puts("*** wav-linq: fatal error (code L11b)");
        return(-1);
    }

    if (_bits_smple == 8 || _bits_smple == 16 || _bits_smple == 24 || _bits_smple == 32); else
    {
        puts("*** wav-linq: fatal error (code L12)");
        return(-1);
    }

    if (_block_algn == _n_channels * _bits_smple / 8); else
    {
        puts("*** wav-linq: fatal error (code L13)");
        return(-1);
    }

    if (_file_data); else
    {
        puts("*** wav-linq: fatal error (code L14)");
        return(-1);
    }

    free(_file_data);
    return(_m_round((double)_data_chunk_length / (_n_channels * _bits_smple / 8) * ((double)_target_frequency / (double)_sampling_r)));
}

// **_wave_file_read** loads the WAV file in `_fname`, resamples it to `_target_frequency`, and stores the samples in `_data`.
int _wave_file_read(char *_fname, int _channel, float *_data, int _target_frequency)
{
    int _hdl, _d;
    char *_read_buff, *_file_data = NULL;
    char _rbf[16];
    char _wav_guid[16];
    char _cc[4];

    int _riff_chunk_size, _chunk_length, _keep_reading = 1, _fmt_parsed = 0;

    int _format_tag, _n_channels, _sampling_r, _bytes_scnd, _block_algn, _bits_smple, _extnsn_len, _data_chunk_length;

    int _org_data_length, _new_data_length;
    float *_bi, *_bo;
    int k;
    void *_handle;
    int _iu, _ou;

    _hdl = _open(_fname, _O_RDONLY | _O_BINARY );
    if (_hdl == -1)
    {
        puts("*** wav-read: fatal error (code R00)");
        return(-1);
    }

    _d = _read(_hdl, _rbf, 0x0C);
    if (_d == 0x0C); else
    {
        puts("*** wav-read: fatal error (code R01)");
        return(-1);
    }

    /* this should have RIFF <RIFF chunk size> WAVE */

    if (_rbf[0x00] == 'R' && _rbf[0x01] == 'I' && _rbf[0x02] == 'F' && _rbf[0x03] == 'F'); else
    {
        puts("*** wav-read: fatal error (code R02)");
        return(-1);
    }

    _riff_chunk_size = *(int *)(_rbf + 0x04); /* what shall we do with the drunken sailor?... */

    if (_rbf[0x08] == 'W' && _rbf[0x09] == 'A' && _rbf[0x0A] == 'V' && _rbf[0x0B] == 'E'); else
    {
        puts("*** wav-read: fatal error (code R03)");
        return(-1);
    }

    /* now we need to read chunks until we find the "data" chunk */
    /* if/when we meet "fmt " chunk, we need to parse it */

    while (_keep_reading)
    {
        _d = _read(_hdl, _rbf, 0x08);
        if (_d == 0x08); else
        {
            puts("*** wav-read: fatal error (code R04)");
            return(-1);
        }

        _chunk_length = *(int *)(_rbf + 0x04);

        _read_buff = (char *)malloc(_chunk_length);

        if (_read_buff); else
        {
            puts("*** wav-read: fatal error (code R05)");
            return(-1);
        }

        _d = _read(_hdl, _read_buff, _chunk_length);
        if (_d == _chunk_length); else
        {
            puts("*** wav-read: fatal error (code R06)");
            return(-1);
        }

        if (_rbf[0x00] == 'f' && _rbf[0x01] == 'm' && _rbf[0x02] == 't' && _rbf[0x03] == ' ')
        {
            if (_chunk_length < 0x10)
            {
                puts("*** wav-read: fatal error (code R07)");
                return(-1);
            }

            _format_tag = *(short *)(_read_buff + 0x00);
            _n_channels = *(short *)(_read_buff + 0x02);
            _sampling_r = *(int *)(_read_buff + 0x04);
            _bytes_scnd = *(int *)(_read_buff + 0x08);
            _block_algn = *(short *)(_read_buff + 0x0C);
            _bits_smple = *(short *)(_read_buff + 0x0E);

            if (_format_tag == -2)
                if (_chunk_length < 0x28)
                {
                    puts("*** wav-read: fatal error (code R07a)");
                    return(-1);
                }
            if (_format_tag == -2) _extnsn_len = *(short *)(_read_buff + 0x10);
            if (_format_tag == -2)
                if (_extnsn_len < 0x16)
                {
                    puts("*** wav-read: fatal error (code R07b)");
                    return(-1);
                }
            if (_format_tag == -2) memcpy(_wav_guid, _read_buff + 0x18, 0x10);

            _fmt_parsed = 1;
        }

        if (_rbf[0x00] == 'd' && _rbf[0x01] == 'a' && _rbf[0x02] == 't' && _rbf[0x03] == 'a')
            if (_fmt_parsed)
            {
                _data_chunk_length = _chunk_length;
                _keep_reading = 0;
                _file_data = (char *)malloc(_data_chunk_length);
                if (_file_data); else
                {
                    puts("*** wav-read: fatal error (code R08)");
                    return(-1);
                }
                memcpy(_file_data, _read_buff, _chunk_length);
            }
            else
            {
                puts("*** wav-read: fatal error (code R09)");
                return(-1);
            }

        free(_read_buff);
    }
    _close(_hdl);

    if (_fmt_parsed); else
    {
        puts("*** wav-read: fatal error (code R10)");
        return(-1);
    }

    if (_format_tag == 1 || _format_tag == 3 || _format_tag == -2); else
    {
        puts("*** wav-read: fatal error (code R11)");
        return(-1);
    }

    if (_format_tag == -2)
        if (!_verify_wav_guid(_wav_guid))
        {
            puts("*** wav-read: fatal error (code R11a)");
            return(-1);
        }

    if (_format_tag == 3)
        if (_bits_smple == 32); else
        {
            puts("*** wav-read: fatal error (code R11c)");
            return(-1);
        }

    if (0 <= _channel && _channel < _n_channels); else
    {
        puts("*** wav-read: fatal error (code R11b)");
        return(-1);
    }

    if (_bits_smple == 8 || _bits_smple == 16 || _bits_smple == 24 || _bits_smple == 32); else
    {
        puts("*** wav-read: fatal error (code R12)");
        return(-1);
    }

    if (_block_algn == _n_channels * _bits_smple / 8); else
    {
        puts("*** wav-read: fatal error (code R13)");
        return(-1);
    }

    if (_file_data); else
    {
        puts("*** wav-read: fatal error (code R14)");
        return(-1);
    }

    _org_data_length = _data_chunk_length / (_n_channels * _bits_smple / 8);

    _new_data_length = _m_round(_org_data_length * ((double)_target_frequency / (double)_sampling_r));

    _bi = (float *)malloc((_org_data_length + 64) * sizeof(float));
    if (_bi); else
    {
        puts("*** wav-read: fatal error (code R15)");
        return(-1);
    }

    if (_format_tag == 1 && _bits_smple == 8) for (k = 0; k < _org_data_length; k++) _bi[k] = ((float)((unsigned char)_file_data[(k * _n_channels + _channel) * _bits_smple / 8]) - 128.0f) / 128.0f;
    if (_format_tag == 1 && _bits_smple == 16)
        for (k = 0; k < _org_data_length; k++)
        {
            _cc[0] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 0];
            _cc[1] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 1];
            _cc[2] = _cc[1] & 0x80 ? 0xFF : 0x00;
            _cc[3] = _cc[2] & 0x80 ? 0xFF : 0x00;
            _bi[k] = (float)((double)(*(int *)_cc) / (double)32768.0 );
        }
    if (_format_tag == 1 && _bits_smple == 24)
        for (k = 0; k < _org_data_length; k++)
        {
            _cc[0] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 0];
            _cc[1] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 1];
            _cc[2] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 2];
            _cc[3] = _cc[2] & 0x80 ? 0xFF : 0x00;
            _bi[k] = (float)((double)(*(int *)_cc) / (double)32768.0 / (double)256.0 );
        }
    if (_format_tag == 1 && _bits_smple == 32)
        for (k = 0; k < _org_data_length; k++)
        {
            _cc[0] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 0];
            _cc[1] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 1];
            _cc[2] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 2];
            _cc[3] = _file_data[(k * _n_channels + _channel) * _bits_smple / 8 + 3];
            _bi[k] = (float)((double)(*(int *)_cc) / (double)32768.0 / (double)256.0 / (double)256.0);
        }

    if (_format_tag == 3) for (k = 0; k < _org_data_length; k++) _bi[k] = *(float *)(_file_data + (k * _n_channels + _channel) * _bits_smple / 8);

    free(_file_data);

    if (_target_frequency == _sampling_r) memcpy(_data, _bi, _org_data_length * sizeof(float));
    if (_target_frequency == _sampling_r) free(_bi);

    if (_target_frequency == _sampling_r) return(0);

    puts("*** wav-read: wave sampling rate differs from playback frequency; resampling...");

    _bo = (float *)malloc((_new_data_length + 64) * sizeof(float));
    if (_bo); else
    {
        puts("*** wav-read: fatal error (code R16)");
        return(-1);
    }

    _handle = resample_open(1, (double)_target_frequency / (double)_sampling_r, (double)_target_frequency / (double)_sampling_r);

    _ou = resample_process(_handle, (double)_target_frequency / (double)_sampling_r,
                           _bi, _org_data_length, 1, &_iu, _bo, _new_data_length);

    resample_close(_handle);
    free(_bi);
    puts("*** wav-read: resampling done");

    if (_iu == _org_data_length); else puts("*** wav-read: WARNING: inconsistency in inp byte count");
    if (_ou == _new_data_length); else puts("*** wav-read: WARNING: inconsistency in out byte count");

    memcpy(_data, _bo, _new_data_length * sizeof(float));
    free(_bo);
    return(0);
}

// **_mp3_file_length_inquiry** determines the number of samples in the specified MP3 file if sampled at `_target_frequency`.
int _mp3_file_length_inquiry(char *_fname, int _channel, int _target_frequency)
{
    mpg123_handle *_mh = NULL;
    int _d;
    int _sampling_r, _n_channels, _mpg123_encoding;
    float *_mpg123_data_block;
    size_t _output_byte_count;
    int _sample_count = 0;
    int _keep_reading = 1;

    if (mpg123_init())
    {
        puts("*** mp3-linq: fatal error (code N00)");
        return(-1);
    }

    _mh = mpg123_new(NULL, &_d);
    if (_d)
    {
        puts("*** mp3-linq: fatal error (code N01a)");
        return(-1);
    }

    if (_mh); else
    {
        puts("*** mp3-linq: fatal error (code N01b)");
        return(-1);
    }

    if (mpg123_open(_mh, _fname))
    {
        puts("*** mp3-linq: fatal error (code N02)");
        return(-1);
    }

    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_QUIET, 0.0))
    {
        puts("*** mp3-linq: fatal error (code N03a)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_SKIP_ID3V2, 0.0))
    {
        puts("*** mp3-linq: fatal error (code N03b)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_FORCE_FLOAT, 0.0))
    {
        puts("*** mp3-linq: fatal error (code N03c)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_IGNORE_INFOFRAME, 0.0))
    {
        puts("*** mp3-linq: fatal error (code N03d)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_IGNORE_STREAMLENGTH, 0.0))
    {
        puts("*** mp3-linq: fatal error (code N03a)");
        return(-1);
    }

    if (mpg123_getformat(_mh, &_sampling_r, &_n_channels, &_mpg123_encoding))
    {
        puts("*** mp3-linq: fatal error (code N04)");
        return(-1);
    }

    if (_mpg123_encoding == MPG123_ENC_FLOAT_32); else
    {
        puts("*** mp3-linq: fatal error (code N05)");
        return(-1);
    }

    if (_sampling_r && _n_channels); else
    {
        puts("*** mp3-linq: fatal error (code N06)");
        return(-1);
    }

    if (0 <= _channel && _channel < _n_channels); else
    {
        puts("*** mp3-linq: fatal error (code N07)");
        return(-1);
    }

    if (mpg123_format_none(_mh))
    {
        puts("*** mp3-linq: fatal error (code N08)");
        return(-1);
    }

    if (mpg123_format(_mh, _sampling_r, _n_channels, _mpg123_encoding))
    {
        puts("*** mp3-linq: fatal error (code N09)");
        return(-1);
    }

    while (_keep_reading)
    {
        _mpg123_data_block = (float *)malloc(CD_SAMPLES_PER_SECOND * _n_channels * sizeof(float));
        if (_mpg123_data_block); else
        {
            puts("*** mp3-linq: fatal error (code N10a)");
            return(-1);
        }

        _d = mpg123_read(_mh, (unsigned char *)_mpg123_data_block, CD_SAMPLES_PER_SECOND * _n_channels * sizeof(float), &_output_byte_count);

        free(_mpg123_data_block);
        _sample_count += (int)_output_byte_count / (_n_channels * sizeof(float));

        if (_d == MPG123_NEED_MORE)
        {
            puts("*** mp3-linq: WARNING: .mp3 file appears to be truncated, will play as is");
            _keep_reading = 0;
            continue;
        }

        if (_d == MPG123_NEW_FORMAT)
        {
            puts("*** mp3-linq: fatal error (code N11)");
            return(-1);
        }

        if (_d == MPG123_DONE)
        {
            _keep_reading = 0;
            continue;
        }

        if (_d)
        {
            puts("*** mp3-linq: fatal error (code N12)");
            return(-1);
        }
    }
    mpg123_close(_mh);
    mpg123_delete(_mh);
    mpg123_exit();

    return(_m_round((double)_sample_count * ((double)_target_frequency / (double)_sampling_r)));
}

// **_mp3_file_read** loads the MP3 file in `_fname`, resamples it to `_target_frequency`, and stores the samples in `_data`.
int _mp3_file_read(char *_fname, int _channel, float *_data, int _target_frequency)
{
    mpg123_handle *_mh = NULL;
    int _d;
    int _sampling_r, _n_channels, _mpg123_encoding;
    float *_mpg123_data_block;
    size_t _output_byte_count;
    int _sample_count = 0;
    int _keep_reading = 1;

    struct _mp3dta *_list_head = NULL, *_work;
    float *_bi, *_bo;
    int k;
    int _ip = 0;

    int _org_data_length, _new_data_length;
    void *_handle;
    int _iu, _ou;

    if (mpg123_init())
    {
        puts("*** mp3-read: fatal error (code M00)");
        return(-1);
    }

    _mh = mpg123_new(NULL, &_d);
    if (_d)
    {
        puts("*** mp3-read: fatal error (code M01a)");
        return(-1);
    }

    if (_mh); else
    {
        puts("*** mp3-read: fatal error (code M01b)");
        return(-1);
    }

    if (mpg123_open(_mh, _fname))
    {
        puts("*** mp3-read: fatal error (code M02)");
        return(-1);
    }

    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_QUIET, 0.0))
    {
        puts("*** mp3-read: fatal error (code M03a)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_SKIP_ID3V2, 0.0))
    {
        puts("*** mp3-read: fatal error (code M03b)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_FORCE_FLOAT, 0.0))
    {
        puts("*** mp3-read: fatal error (code M03c)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_IGNORE_INFOFRAME, 0.0))
    {
        puts("*** mp3-read: fatal error (code M03d)");
        return(-1);
    }
    if (mpg123_param(_mh, MPG123_ADD_FLAGS, MPG123_IGNORE_STREAMLENGTH, 0.0))
    {
        puts("*** mp3-read: fatal error (code M03a)");
        return(-1);
    }

    if (mpg123_getformat(_mh, &_sampling_r, &_n_channels, &_mpg123_encoding))
    {
        puts("*** mp3-read: fatal error (code M04)");
        return(-1);
    }

    if (_mpg123_encoding == MPG123_ENC_FLOAT_32); else
    {
        puts("*** mp3-read: fatal error (code M05)");
        return(-1);
    }

    if (_sampling_r && _n_channels); else
    {
        puts("*** mp3-read: fatal error (code M06)");
        return(-1);
    }

    if (0 <= _channel && _channel < _n_channels); else
    {
        puts("*** mp3-read: fatal error (code M07)");
        return(-1);
    }

    if (mpg123_format_none(_mh))
    {
        puts("*** mp3-read: fatal error (code M08)");
        return(-1);
    }

    if (mpg123_format(_mh, _sampling_r, _n_channels, _mpg123_encoding))
    {
        puts("*** mp3-read: fatal error (code M09)");
        return(-1);
    }

    while (_keep_reading)
    {
        _mpg123_data_block = (float *)malloc(CD_SAMPLES_PER_SECOND * _n_channels * sizeof(float));
        if (_mpg123_data_block); else
        {
            puts("*** mp3-read: fatal error (code M10a)");
            return(-1);
        }

        _d = mpg123_read(_mh, (unsigned char *)_mpg123_data_block, CD_SAMPLES_PER_SECOND * _n_channels * sizeof(float), &_output_byte_count);

        _work = (struct _mp3dta *)malloc(sizeof(struct _mp3dta));
        if (_work); else
        {
            puts("*** mp3-read: fatal error (code M10b)");
            return(-1);
        }

        _work->_data = _mpg123_data_block;
        _work->_scnt = (int)_output_byte_count / (_n_channels * sizeof(float));
        _work->_next = _list_head;
        _list_head = _work;

        _sample_count += _output_byte_count / (_n_channels * sizeof(float));

        if (_d == MPG123_NEED_MORE)
        {
            puts("*** mp3-read: WARNING: .mp3 file appears to be truncated, will play as is");
            _keep_reading = 0;
            continue;
        }

        if (_d == MPG123_NEW_FORMAT)
        {
            puts("*** mp3-read: fatal error (code M11)");
            return(-1);
        }

        if (_d == MPG123_DONE)
        {
            _keep_reading = 0;
            continue;
        }

        if (_d)
        {
            puts("*** mp3-read: fatal error (code M12)");
            return(-1);
        }
    }
    mpg123_close(_mh);
    mpg123_delete(_mh);
    mpg123_exit();

    _org_data_length = _sample_count;

    _new_data_length = _m_round(_org_data_length * ((double)_target_frequency / (double)_sampling_r));

    _bi = (float *)malloc(_sample_count * sizeof(float));
    if (_bi); else
    {
        puts("*** mp3-read: fatal error (code M13)");
        return(-1);
    }

    _work = _list_head;
    _ip = _sample_count;
    while (_work)
    {
        _ip -= _work->_scnt;

        for (k = 0; k < _work->_scnt; k++) _bi[_ip + k] = _work->_data[k * _n_channels + _channel];

        free(_work->_data);
        _work = _work->_next;
    }

    if (_target_frequency == _sampling_r) memcpy(_data, _bi, _org_data_length * sizeof(float));
    if (_target_frequency == _sampling_r) free(_bi);
    if (_target_frequency == _sampling_r) return(0);

    puts("*** mp3-read: mp3 sampling rate differs from playback frequency; resampling...");

    _bo = (float *)malloc((_new_data_length + 64) * sizeof(float));
    if (_bo); else
    {
        puts("*** mp3-read: fatal error (code M14)");
        return(-1);
    }

    _handle = resample_open(1, (double)_target_frequency / (double)_sampling_r, (double)_target_frequency / (double)_sampling_r);

    _ou = resample_process(_handle, (double)_target_frequency / (double)_sampling_r,
                           _bi, _org_data_length, 1, &_iu, _bo, _new_data_length);

    resample_close(_handle);
    free(_bi);
    puts("*** mp3-read: resampling done");

    if (_iu == _org_data_length); else puts("*** mp3-read: WARNING: inconsistency in inp byte count");
    if (_ou == _new_data_length); else puts("*** mp3-read: WARNING: inconsistency in out byte count");

    memcpy(_data, _bo, _new_data_length * sizeof(float));
    free(_bo);
    return(0);
}

// **read_sound_file_length** determines the number of samples in the specified WAV/MP3 file if sampled at `target_frequency`.
int read_sound_file_length(char *filename, int channel, int target_frequency)
{
    int _p;
    int _f = -1;

    _p = strlen(filename) - 4;

    if (_p < 0); else if (filename[_p] == '.' && tolower(filename[_p + 1]) == 'm' && tolower(filename[_p + 2]) == 'p' && tolower(filename[_p + 3]) == '3') _f = 0;
    if (_p < 0); else if (filename[_p] == '.' && tolower(filename[_p + 1]) == 'w' && tolower(filename[_p + 2]) == 'a' && tolower(filename[_p + 3]) == 'v') _f = 1;

    if ( _f == -1) puts("*** snd-read: WARNING: unknown file extension, reading as .wav file...");

    if (!_f) return(_mp3_file_length_inquiry(filename, channel, target_frequency));

    return(_wave_file_length_inquiry(filename, channel, target_frequency));
}

// **read_sound_file_data** loads the WAV/MP3 file in `filename`, resamples it to `target_frequency`, and stores the samples in `data`.
int read_sound_file_data(char *filename, int channel, float *data, int target_frequency)
{
    int _p;
    int _f = -1;

    _p = strlen(filename) - 4;

    if (_p < 0); else if (filename[_p] == '.' && tolower(filename[_p + 1]) == 'm' && tolower(filename[_p + 2]) == 'p' && tolower(filename[_p + 3]) == '3') _f = 0;
    if (_p < 0); else if (filename[_p] == '.' && tolower(filename[_p + 1]) == 'w' && tolower(filename[_p + 2]) == 'a' && tolower(filename[_p + 3]) == 'v') _f = 1;

    if ( _f == -1) puts("*** snd-read: WARNING: unknown file extension, reading as .wav file...");

    if (!_f) return(_mp3_file_read(filename, channel, data, target_frequency));

    return(_wave_file_read(filename, channel, data, target_frequency));
}