#ifndef VS_RENDER_H
#define VS_RENDER_H

#define	GLEW_STATIC	1
#define	FREEGLUT_STATIC	1

#include "GL/glew.h"
#include "GL/freeglut.h"

#include "constants.h"
#define	_MIN_DRAW_RANGE	  0.125
#define	_MAX_DRAW_RANGE	256.000

void init_gl(void);
void draw_room(void);
void draw_source(float source_x, float source_y, float source_z);

#endif